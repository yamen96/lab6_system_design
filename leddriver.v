module leddriver (ledin, ledout);

input [3:0] ledin;
output reg [6:0] ledout;
		
   // Your code for Phase 2 goes here.  Be sure to check the Slide Set 2 notes,
   // since one of the slides almost gives away the answer here.  I wrote this as
   // a single combinational always block containing a single case statement, but 
   // there are other ways to do it.
	
	always@(*)
		case(ledin)
			4'b0000: ledout = 7'b1000000; 
 			4'b0001: ledout = 7'b1111001; 
 			4'b0010: ledout = 7'b0100100; 
 			4'b0011: ledout = 7'b0110000; 
 			4'b0100: ledout = 7'b0011001; 
 			4'b0101: ledout = 7'b0010010; 
 			4'b0110: ledout = 7'b0000010; 
 			4'b0111: ledout = 7'b1111000; 
 			4'b1000: ledout = 7'b0000000; 
 			4'b1001: ledout = 7'b0010000; 
 			4'b1010: ledout = 7'b0001000; 
 			4'b1011: ledout = 7'b0000011; 
 			4'b1100: ledout = 7'b1000110; 
 			4'b1101: ledout = 7'b0100001; 
 			4'b1110: ledout = 7'b0000110; 
 			4'b1111: ledout = 7'b0001110; 
 			default: ledout = 7'b1111111; 
		endcase
endmodule
