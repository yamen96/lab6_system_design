	component nios_system is
		port (
			clk_clk         : in  std_logic                    := 'X';             -- clk
			ledin_export    : out std_logic_vector(7 downto 0);                    -- export
			leds_export     : out std_logic_vector(7 downto 0);                    -- export
			reset_reset_n   : in  std_logic                    := 'X';             -- reset_n
			switches_export : in  std_logic_vector(7 downto 0) := (others => 'X'); -- export
			keys_export     : in  std_logic_vector(3 downto 0) := (others => 'X'); -- export
			freq_export     : out std_logic_vector(9 downto 0)                     -- export
		);
	end component nios_system;

	u0 : component nios_system
		port map (
			clk_clk         => CONNECTED_TO_clk_clk,         --      clk.clk
			ledin_export    => CONNECTED_TO_ledin_export,    --    ledin.export
			leds_export     => CONNECTED_TO_leds_export,     --     leds.export
			reset_reset_n   => CONNECTED_TO_reset_reset_n,   --    reset.reset_n
			switches_export => CONNECTED_TO_switches_export, -- switches.export
			keys_export     => CONNECTED_TO_keys_export,     --     keys.export
			freq_export     => CONNECTED_TO_freq_export      --     freq.export
		);

