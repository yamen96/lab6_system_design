
module nios_system (
	clk_clk,
	ledin_export,
	leds_export,
	reset_reset_n,
	switches_export,
	keys_export,
	freq_export);	

	input		clk_clk;
	output	[7:0]	ledin_export;
	output	[7:0]	leds_export;
	input		reset_reset_n;
	input	[7:0]	switches_export;
	input	[3:0]	keys_export;
	output	[9:0]	freq_export;
endmodule
