	nios_system u0 (
		.clk_clk         (<connected-to-clk_clk>),         //      clk.clk
		.ledin_export    (<connected-to-ledin_export>),    //    ledin.export
		.leds_export     (<connected-to-leds_export>),     //     leds.export
		.reset_reset_n   (<connected-to-reset_reset_n>),   //    reset.reset_n
		.switches_export (<connected-to-switches_export>), // switches.export
		.keys_export     (<connected-to-keys_export>),     //     keys.export
		.freq_export     (<connected-to-freq_export>)      //     freq.export
	);

